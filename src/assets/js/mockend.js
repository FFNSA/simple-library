let db;
const indexedDB =
    window.indexedDB ||
    window.mozIndexedDB ||
    window.webkitIndexedDB ||
    window.msIndexedDB ||
    window.shimIndexedDB;
const TX = {
    READONLY: "readonly",
    READWRITE: "readwrite"
};
const USERS = "users";
const BOOKS = "books";

let request = indexedDB.open("myAwesomeDB", 1);

function _createTables() {
    const books = db.createObjectStore(BOOKS, {
        keyPath: "_id",
        autoIncrement: true
    });
    const users = db.createObjectStore(USERS, {
        keyPath: "_id",
        autoIncrement: true
    });
}

export default function init() {
    return new Promise((success, err) => {
        console.debug("init");
        request.onerror = (event) => {
            console.debug("Error");
            err("Impossible to create the db");
        };

        request.onsuccess = (event) => {
            db = request.result;
            console.debug("Creation db: ", event);
            success(true);
        };
        request.onupgradeneeded = () => {
            console.debug("on upgare");
            db = request.result;
            _createTables(db);
            success(true);
        };
    });
}

async function login({username, password}) {
    return new Promise((success, err) => {
        if (username === "admin" && password === "password") {
            success({token: _uuidv4()});
        } else {
            err({error: "username or password are wrong"});
        }
    });
}

async function addUser({name, surname, age, gender, height}) {
    return new Promise((success, err) => {
        const tx = db.transaction(USERS, TX.READWRITE);
        const users = tx.objectStore(USERS);
        const user = {
            name,
            surname,
            age,
            gender,
            height,
            created: new Date()
        };
        const request = users.add(user);
        request.onsuccess = () => {
            success({_id: request.result});
        };
    });
}

async function getUserById({_id}) {
    return new Promise((success, err) => {
        const tx = db.transaction(USERS, TX.READONLY);
        const users = tx.objectStore(USERS);
        const user = users.get(_id);

        user.onsuccess = () => {
            console.debug(user);
            if (user.result) {
                success(user.result);
            } else {
                err({error: "userNotFound"});
            }
        };
        user.onerror = (event) => {
            console.debug(event);
            err(event);
        };
    });
}

async function getAllUsers() {
    return new Promise((success, err) => {
        const tx = db.transaction(USERS, TX.READONLY);
        const users = tx.objectStore(USERS);
        const user = users.getAll();

        user.onsuccess = () => {
            success(
                user.result.map((u) => ({
                    _id: u._id,
                    name: u.name,
                    surname: u.surname
                }))
            );
        };
        user.onerror = (event) => {
            console.debug(user.result);
            err(event);
        };
    });
}

async function getUserByNameSurname({name = "", surname = ""}) {
    return new Promise((success, err) => {
        const tx = db.transaction(USERS, TX.READONLY);
        const users = tx.objectStore(USERS);
        const request = users.openCursor();
        let usersResult = [];
        request.onsuccess = (event) => {
            const cursor = event.target.result;
            if (cursor) {
                usersResult = [...usersResult, cursor.value];
                cursor.continue();
            } else {
                success(
                    usersResult.filter(
                        (u) => u.name.indexOf(name) >= 0 && u.surname.indexOf(surname) >= 0
                    )
                );
            }
        };
    });
}

async function deleteById({_id}) {
    console.debug(_id);
    return new Promise(async (success, err) => {
        try {
            const user = await getUserById({_id});
            const tx = db.transaction(USERS, TX.READWRITE);
            const users = tx.objectStore(USERS);
            const request = users.delete(_id);
            request.onsuccess = (event) => {
                console.debug(event);
                success({deleted: true});
            };
        } catch (e) {
            console.debug(e);
            err({error: "User id doesnt exist!"});
        }
    });
}

async function updateUser({
                              _id,
                              name = "",
                              surname = "",
                              age = -1,
                              gender = "",
                              height = -1
                          }) {
    const userEdit = {_id, name, surname, age, gender, height};
    return new Promise(async (success, err) => {
        try {
            const user = await getUserById({_id});
            const tx = db.transaction(USERS, TX.READWRITE);
            const users = tx.objectStore(USERS);
            Object.keys(userEdit).forEach((k) => {
                if (!userEdit[k]) {
                    delete userEdit[k];
                }
                if (userEdit[k] === -1) {
                    delete userEdit[k];
                }
            });
            const newUser = {
                ...user,
                ...userEdit
            };
            const request = users.put(newUser);
            request.onsuccess = (event) => {
                success({result: `User ${_id} updated`});
            };
        } catch (e) {
            console.debug(e);
            err({error: "User id doesnt exist!"});
        }
    });
}

// TODO BOOKS

async function addBook({name, price, author, isEbook, userId}) {
    return new Promise((success, err) => {
        const tx = db.transaction(BOOKS, TX.READWRITE);
        const books = tx.objectStore(BOOKS);
        const book = {
            name,
            price,
            author,
            isEbook,
            userId,
            created: new Date()
        };
        const request = books.add(book);
        request.onsuccess = () => {
            success({_id: request.result});
        };
    });
}

async function getBookById({_id}) {
    return new Promise((success, err) => {
        const tx = db.transaction(BOOKS, TX.READONLY);
        const books = tx.objectStore(BOOKS);
        const book = books.get(_id);

        book.onsuccess = () => {
            console.debug(book.result);
            if (book.result) {
                success(book.result);
            } else {
                err({error: "bookNotFound"});
            }
        };
        book.onerror = (event) => {
            console.debug(book.result);
            err(event);
        };
    });
}

async function getAllBooks() {
    return new Promise((success, err) => {
        const tx = db.transaction(BOOKS, TX.READONLY);
        const books = tx.objectStore(BOOKS);
        const book = books.getAll();

        book.onsuccess = () => {
            console.debug(book.result);
            success(book.result);
        };
        book.onerror = (event) => {
            console.debug(book.result);
            err(event);
        };
    });
}

async function getBookByNameAuthor({userId, name = "", author = ""}) {
    return new Promise((success, err) => {
        const tx = db.transaction(BOOKS, TX.READONLY);
        const books = tx.objectStore(BOOKS);
        const request = books.openCursor();
        let booksResult = [];
        request.onsuccess = (event) => {
            const cursor = event.target.result;
            if (cursor) {
                booksResult = [...booksResult, cursor.value];
                cursor.continue();
            } else {
                success(
                    booksResult.filter((b) => {
                        return (
                            b.userId === userId &&
                            b.name.indexOf(name) >= 0 &&
                            b.author.indexOf(author) >= 0
                        );
                    })
                );
            }
        };
    });
}

async function getBooksByUserId({userId}) {
    return new Promise((success, err) => {
        const tx = db.transaction(BOOKS, TX.READONLY);
        const books = tx.objectStore(BOOKS);
        const request = books.openCursor();
        let booksResult = [];
        request.onsuccess = (event) => {
            const cursor = event.target.result;
            if (cursor) {
                booksResult = [...booksResult, cursor.value];
                cursor.continue();
            } else {
                success(
                    booksResult.filter((b) => {
                        return b.userId === userId;
                    })
                );
            }
        };
    });
}

async function deleteBookById({_id}) {
    console.debug(_id);
    return new Promise(async (success, err) => {
        try {
            const book = await getBookById({_id});
            const tx = db.transaction(BOOKS, TX.READWRITE);
            const books = tx.objectStore(BOOKS);

            const request = books.delete(_id);
            request.onsuccess = (event) => {
                console.debug(event);
                success({deleted: true});
            };
        } catch (e) {
            console.debug(e);
            err({error: "book id doesnt exist!"});
        }
    });
}

async function updateBook({
                              _id,
                              userId = "",
                              name = "",
                              price = "",
                              author = "",
                              isEbook = ""
                          }) {
    const bookEdit = {_id, name, price, author, isEbook, userId};
    return new Promise(async (success, err) => {
        try {
            const book = await getBookById({_id});
            const tx = db.transaction(BOOKS, TX.READWRITE);
            const books = tx.objectStore(BOOKS);
            Object.keys(bookEdit).forEach((k) => {
                if (!bookEdit[k]) {
                    delete bookEdit[k];
                }
                if (bookEdit[k] === -1) {
                    delete bookEdit[k];
                }
            });
            const newBook = {
                ...book,
                ...bookEdit
            };
            const request = books.put(newBook);
            request.onsuccess = (event) => {
                success({result: `Book ${_id} updated`});
            };
        } catch (e) {
            console.debug(e);
            err({error: "User id doesnt exist!"});
        }
    });
}

export const api = {
    users: {
        addUser,
        getUserById,
        getUserByNameSurname,
        getAllUsers,
        deleteById,
        updateUser
    },
    auth: {
        login
    },
    books: {
        addBook,
        getBookById,
        getAllBooks,
        getBookByNameAuthor,
        deleteBookById,
        getBooksByUserId
    }
};

function _uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
        (
            c ^
            (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
        ).toString(16)
    );
}
