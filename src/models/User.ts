export interface User {
    _id: number;
    name: string;
    surname: string;
    age: number;
    gender: 'M' | 'F' | 'X';
    height: number;
    created: Date | number;
}
