export interface Book {
    _id: number;
    name: string;
    price: number;
    author: string;
    isEbook: boolean;
    userId: number;
    created: Date;
}
