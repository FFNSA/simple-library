/**
 * Capitalize input string
 * @param string - String to be capitalized
 */
export function capitalize(string: string): string {
    return string.replace(/\w\S*/g, (c) => c.charAt(0).toUpperCase() + c.substr(1).toLowerCase())
}
