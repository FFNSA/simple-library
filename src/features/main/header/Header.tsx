import React from "react";
import logo from "../../../assets/img/logo.svg";
import "./Header.css";
import {useNavigate} from "react-router-dom";

export default function Header() {

    const navigate = useNavigate();

    return (
        <header>
            <div className="header-block" onClick={() => navigate("/")}>
                <img className="logo" alt="logo" src={logo}/>
                <span className="title">Simple Library</span>
            </div>
        </header>
    );
}
