import React from "react";
import "./Footer.css";

export default function Footer() {
    return (
        <footer>
            <small>Final project of the React Course issued by nesea during Fall 2021. Developed by Fabrizio Finucci</small>
        </footer>
    );
}
