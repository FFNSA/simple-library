import React from "react";
import {NavLink} from "react-router-dom";
import "./Nav.css";
import {useDispatch, useSelector} from "react-redux";
import {resetAuthState, selectAuthState} from "../../login/auth.slice";

export default function Nav() {

    const dispatch = useDispatch();
    const isAuth = useSelector(selectAuthState);

    /**
     * Exit the application
     */
    function logout() {
        dispatch(resetAuthState());
    }

    return (
        <nav className="nav nav-tabs justify-content-center">
            {
                isAuth &&
                <>
                    <NavLink to="/books"
                             className={({isActive}) => isActive ? "nav-link active" : "nav-link"}>Books</NavLink>
                    <NavLink to="/users"
                             className={({isActive}) => isActive ? "nav-link active" : "nav-link"}>Users</NavLink>
                    <NavLink to="/login" className="nav-link" onClick={logout}>Logout</NavLink>
                </>
            }
        </nav>
    );
}
