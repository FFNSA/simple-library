import React from "react";
import "./NotFound.css";

export default function NotFound() {
    return (
        <section className="not-found pt-5">
            <div className="row justify-content-center">
                <div className="col-8">
                    <h2>404 - Not found</h2>
                </div>
            </div>
        </section>
    );
}
