import {createSlice} from "@reduxjs/toolkit";
import {api} from "../../assets/js/mockend";

const initialState = {
    attempts: 0,
    isAuthenticated: false,
    error: '',
    token: ''
};

export const authSlice = createSlice({
    name: 'auth',
    initialState: initialState,
    reducers: {
        incrementAttempts: state => {
            state.attempts += 1;
        },
        resetAuthState: () => {
            sessionStorage.removeItem('token');
            return initialState;
        },
        setAuthError: (state, {payload}) => {
            state.error = payload.error;
        },
        setAuthState: (state, {payload}) => {
            state.error = payload.error || '';
            state.token = payload.token;
            state.isAuthenticated = !!payload.token;

            // Saving session to avoid multiple logins on app refresh
            sessionStorage.setItem('token', payload.token);
        }
    }
});

export const {resetAuthState, setAuthError} = authSlice.actions;

/**
 * Thunk for user's authentication
 * @param username - Username credential
 * @param password - Password credential
 */
export const authenticate = (username?: string, password?: string) => {
    return async (dispatch: (fn: { payload?: { token: string }; type: string; }) => void) => {
        try {
            const setAuthState = authSlice.actions.setAuthState;
            const sessionStorageToken = sessionStorage.getItem('token');

            // Do not call the login api if the user is still logged in
            if (!!sessionStorageToken) {
                const token = sessionStorage.getItem('token');
                dispatch(setAuthState({token}));
            } else if (!!username && !!password) {
                const response = await api.auth.login({username, password});
                const token = response.token;
                dispatch(setAuthState({token}));
            }
        } catch ({error}) {
            console.error('[Login] ', error);
            const setError = authSlice.actions.setAuthError;
            dispatch(setError({error}));
        } finally {
            const incrementAttempts = authSlice.actions.incrementAttempts;
            dispatch(incrementAttempts());
        }
    };
};

// Selectors
export const selectAuthAttempts = (state: { auth: { attempts: any; }; }) => state.auth.attempts;
export const selectAuthError = (state: { auth: { error: string; }; }) => state.auth.error;
export const selectAuthState = (state: { auth: { isAuthenticated: boolean; }; }) => state.auth.isAuthenticated;

export default authSlice.reducer;
