import React, {useEffect, useState} from "react";
import "./Login.css";
import {useForm} from "react-hook-form";
import {useDispatch, useSelector,} from "react-redux";
import {authenticate, selectAuthError, selectAuthState, setAuthError} from "./auth.slice";
import {capitalize} from "../../utils/utils";
import {useNavigate} from "react-router-dom";

export default function Login() {

    const authError = useSelector(selectAuthError);
    const dispatch = useDispatch();
    const isAuth = useSelector(selectAuthState);
    const navigate = useNavigate();

    const {formState: {errors}, handleSubmit, register} = useForm();
    const [inputType, setInputType] = useState('password');

    // Redirect to root path after login. Additional routing provided by the App component
    useEffect(() => {
        if (isAuth) {
            navigate("/", {replace: true});
        }
    }, [isAuth, navigate]);

    /**
     * Perform login using the form data
     * @param data - Username/Password couple to be validated for access
     */
    function login(data: { username: string, password: string }) {
        dispatch(authenticate(data.username, data.password));
    }

    /**
     * Hide the associated alert if an input is clicked
     */
    function hideLoginError() {
        if (!!authError) {
            dispatch(setAuthError({error: ''}));
        }
    }

    /**
     * Show/hide password visibility in the homonym input field
     */
    function togglePasswordVisibility() {
        inputType === 'text' ? setInputType('password') : setInputType('text');
    }

    return (
        <div className="container pt-5">
            <div className="row justify-content-center">
                <div className="col-12 col-md-4">

                    {/*Login form*/}
                    <form onSubmit={handleSubmit(login)}>
                        <div className="mb-3">
                            <input className={`form-control ${errors.username && 'is-invalid'}`}
                                   type="text"
                                   id="username"
                                   placeholder="Username"
                                   {...register("username", {
                                       required: "Username is required",
                                   })}
                                   onClick={hideLoginError}
                                   autoFocus={true}/>
                            {errors.username && <small className="invalid-feedback">{errors.username?.message}</small>}
                        </div>
                        <div className="input-group mb-3">
                            <input className={`form-control ${errors.password && 'is-invalid'}`}
                                   type={inputType}
                                   id="password"
                                   placeholder="Password"
                                   {...register("password", {
                                       required: "Password is required",
                                   })}
                                   onClick={hideLoginError}
                                   autoComplete={'off'}/>
                            <button className="btn btn-outline-secondary btn-password" type="button"
                                    id="toggle-password-visibility" onClick={togglePasswordVisibility}>
                                {
                                    inputType === 'password'
                                        ? <i className="bi bi-eye"/>
                                        : <i className="bi bi-eye-slash"/>
                                }
                            </button>
                            {errors.password && <small className="invalid-feedback">{errors.password?.message}</small>}
                        </div>
                        <button className="btn btn-primary btn-login" type="submit">Login</button>
                    </form>

                    {/*Display the error returned from BE in case of failed authentication*/}
                    {authError && <div className="mt-3 alert alert-danger">{capitalize(authError)}</div>}
                </div>
            </div>
        </div>
    );
}
