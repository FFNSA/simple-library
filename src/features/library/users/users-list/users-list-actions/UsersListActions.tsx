import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {useNavigate} from "react-router-dom";
import {deleteUserById, fetchUserById} from "../../users.slice";

interface UsersListActionsProps {
    id: number;
}

export default function UsersListActions({id}: UsersListActionsProps) {

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [deletionInProgress, setDeletionInProgress] = useState<boolean>();

    /**
     * Update user with provided id
     * @param _id - Identifier of the user to be updated
     */
    function editUser(_id: number) {
        dispatch(fetchUserById(_id))
    }

    /**
     * Delete user with provided id
     * @param _id - Identifier of the user to be deleted
     */
    function deleteUser(_id: number) {
        dispatch(deleteUserById(_id));
    }

    return (
        <div className="row justify-content-end">
            <div className="col-md-auto">
                {
                    !deletionInProgress
                        // Actions buttons
                        ? <>
                            <button className="btn btn-outline-primary btn-sm me-md-2"
                                    onClick={() => editUser(id)} title="Edit">
                                <i className="bi bi-pencil"/>
                            </button>
                            <button className="btn btn-outline-danger btn-sm me-md-2"
                                    onClick={() => setDeletionInProgress(true)} title="Delete">
                                <i className="bi bi-trash"/>
                            </button>
                            <button className="btn btn-outline-secondary btn-sm"
                                    onClick={() => navigate(`/users/${id}`)}
                                    title="Detail">
                                <i className="bi bi-file-text"/>
                            </button>
                        </>

                        // Deletion confirm buttons
                        : <div className="btn-group" role="group" aria-label="Deletion confirmation">
                            <button className="btn btn-sm btn-outline-success" onClick={() => deleteUser(id)} type="button">
                                <i className="bi bi-check2"/>
                            </button>
                            <button className="btn btn-sm btn-outline-danger" onClick={() => setDeletionInProgress(false)}
                                    type="button">
                                <i className="bi bi-x-lg"/>
                            </button>
                        </div>
                }
            </div>
        </div>
    );
}
