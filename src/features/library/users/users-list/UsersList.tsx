import React from "react";
import "./UsersList.css";
import {User} from "../../../../models/User";
import UsersListActions from "./users-list-actions/UsersListActions";

interface UsersListProps {
    users: User[];
}

export default function UsersList({users}: UsersListProps) {

    return (
        <table className="table table-striped table-hover users-list">
            <thead className="table-dark">
            <tr>
                <th scope="col">
                    #
                </th>
                <th scope="col">
                    Name
                </th>
                <th scope="col">
                    Surname
                </th>
                <th scope="col"/>
            </tr>
            </thead>
            <tbody>
            {
                users.map((v, i) =>
                    <tr key={Date.now() + i}>
                        <td>
                            {v._id}
                        </td>
                        <td>
                            {v.name}
                        </td>
                        <td>
                            {v.surname}
                        </td>
                        <td>
                            <UsersListActions id={v._id}/>
                        </td>
                    </tr>
                )
            }
            </tbody>
        </table>
    );
}
