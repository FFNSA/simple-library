import React from "react";
import "./UsersSearch.css";
import {useForm} from "react-hook-form";
import {useDispatch} from "react-redux";
import {fetchUserByNameSurname, fetchUsers} from "../users.slice";
import {capitalize} from "../../../../utils/utils";

export default function UsersSearch() {

    const dispatch = useDispatch();
    const {formState: {errors}, handleSubmit, register, reset} = useForm();

    /**
     * Perform users search by name and surname (both mandatory)
     * @param data - User's data to query (name and surname)
     */
    function submit(data: { name: string, surname: string }) {
        dispatch(fetchUserByNameSurname(data.name, data.surname));
    }

    /**
     * Reset form and restore the original users list
     */
    function cancel() {
        reset();
        dispatch(fetchUsers());
    }

    return (
        <div className="row">
            <div className="col-md-12">
                <form onSubmit={handleSubmit(submit)}>
                    <div className="input-group mb-3">
                        <span className="input-group-text">Search</span>
                        <input aria-label="Name"
                               className={`form-control capitalized ${errors.name && 'is-invalid'}`}
                               id="name"
                               placeholder="Name"
                               type="text"
                               {...register('name', {
                                   required: true,
                                   setValueAs: value => capitalize(value)
                               })}
                        />
                        <input aria-label="Surname"
                               className={`form-control capitalized ${errors.surname && 'is-invalid'}`}
                               id="surname"
                               placeholder="Surname"
                               type="text"
                               {...register('surname', {
                                   required: true,
                                   setValueAs: value => capitalize(value)
                               })}
                        />
                        <button className="btn btn-outline-secondary" type="submit">
                            <i className="bi bi-search"/>
                        </button>
                        <button className="btn btn-outline-danger" onClick={cancel} type="reset">
                            <i className="bi bi-x-lg"/>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
};
