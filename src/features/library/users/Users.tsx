import React, {useEffect} from "react";
import "./Users.css";
import UserForm from "./user-form/UserForm";
import {useDispatch, useSelector} from "react-redux";
import {fetchUsers, selectUsers} from "./users.slice";
import UsersList from "./users-list/UsersList";
import {selectIsDbReady} from "../database.slice";
import UsersSearch from "./users-search/UsersSearch";

export default function Users() {

    const dispatch = useDispatch();
    const dbReady = useSelector(selectIsDbReady);
    const users = useSelector(selectUsers);

    // Fetch all users from db
    useEffect(() => {
        if (dbReady) {
            dispatch(fetchUsers());
        }
    }, [dispatch, dbReady]);

    return (
        <section>
            <div className="container pt-5">
                <div className="row justify-content-center">
                    <div className="col">
                        <div className="row">
                            <div className="col-4">
                                <UserForm/>
                            </div>
                            <div className="col-8">
                                <UsersSearch/>
                                {
                                    !users.length
                                        ?
                                        <div className="alert alert-info" role="alert">No users currently in
                                            database
                                        </div>
                                        :
                                        <UsersList users={users}/>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
