import {createSlice} from "@reduxjs/toolkit";
import {User} from "../../../models/User";
import {api} from "../../../assets/js/mockend";

export const usersSlice = createSlice({
    name: 'users',
    initialState: {
        current: {} as User,
        list: [] as User[]
    },
    reducers: {
        addUser: (state, {payload}) => {
            state.list.push(payload);
        },
        addMultipleUsers: (state, {payload}) => {
            state.list = payload;
        },
        removeUser: (state, {payload}) => {
            state.list = state.list.filter(u => u._id !== payload);
        },
        updateUser: (state, {payload}) => {
            state.list = state.list.map(u => u._id === payload._id ? payload : u);
        },
        setCurrentUser: (state, {payload}) => {
            state.current = payload;
        },
        resetCurrentUser: state => {
            state.current = {} as User;
        }
    }

});

export const {resetCurrentUser} = usersSlice.actions;

/**
 * Thunk for fetching all the users currently stored in the database
 */
export const fetchUsers = () => {
    return async (dispatch: (fn: { payload: User[]; type: string; }) => void) => {
        try {
            const addMultipleUsers = usersSlice.actions.addMultipleUsers;
            const users = await api.users.getAllUsers();
            dispatch(addMultipleUsers(users));
        } catch (e) {
            console.error('[FetchUsers] ', e);
        }
    }
};

/**
 * Thunk for adding an user to the database
 * @param user - The user to be added
 */
export const addUser = (user: Partial<User>) => {
    return async (dispatch: (fn: { payload: User; type: string; }) => void) => {
        try {
            const addUser = usersSlice.actions.addUser;
            const result = await api.users.addUser({
                name: user.name,
                surname: user.surname,
                age: user.age,
                gender: user.gender,
                height: user.height
            });
            dispatch(addUser({...user, _id: result._id}))
        } catch (e) {
            console.error('[AddUser] ', e);
        }
    }
};

/**
 * Thunk for deleting an user from the database
 * @param _id - Id of the user to be removed
 */
export const deleteUserById = (_id: number) => {
    return async (dispatch: (fn: { payload: number; type: string; }) => void) => {
        try {
            const deleteUser = usersSlice.actions.removeUser;
            await api.users.deleteById({_id});
            dispatch(deleteUser(_id));
        } catch (e) {
            console.error('[DeleteUserById] ', e);
        }
    }
};

/**
 * Thunk for fetching an user by the specified id
 * @param _id
 */
export const fetchUserById = (_id: number) => {
    return async (dispatch: (fn: { payload: User; type: string; }) => void) => {

        const setCurrentUser = usersSlice.actions.setCurrentUser;

        try {
            let user = await api.users.getUserById({_id});

            // Convert Date to number, since Date isn't serializable
            user = {...user, created: user.created.getTime()};
            dispatch(setCurrentUser(user));
        } catch ({error}) {
            console.error('[FetchUserById] ', error);
            dispatch(setCurrentUser({_id: -1}));
        }
    }
};

/**
 * Thunk for fetching an user by its name and its surname
 * @param name - Name of the user to retrieve
 * @param surname - Surname of the user to retrieve
 */
export const fetchUserByNameSurname = (name: string, surname: string) => {
    return async (dispatch: (fn: { payload: User; type: string; }) => void) => {
        try {
            const addMultipleUsers = usersSlice.actions.addMultipleUsers;
            let users = await api.users.getUserByNameSurname({name, surname});

            // Convert Date to number, since Date isn't serializable
            users = users.map((u: User) => ({...u, created: (u.created as Date).getTime()}));
            dispatch(addMultipleUsers(users));
        } catch (e) {
            console.error('[FetchUserByNameSurname] ', e);
        }
    }
};

/**
 * Thunk for updating the specified user's value
 * @param user
 */
export const updateUser = (user: User) => {
    return async (dispatch: (fn: { payload: User; type: string; }) => void) => {
        try {
            const updateUser = usersSlice.actions.updateUser;
            await api.users.updateUser(user);
            dispatch(updateUser(user));
        } catch (e) {
            console.error('[UpdateUser] ', e);
        }
    }
};

export const selectUsers = (state: { users: { list: User[] } }) => state.users.list;
export const selectCurrentUser = (state: { users: { current: User } }) => state.users.current;

export default usersSlice.reducer;
