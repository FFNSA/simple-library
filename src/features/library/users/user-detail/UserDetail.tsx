import React, {useEffect} from "react";
import "./UserDetail.css";
import {useNavigate, useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {fetchUserById, resetCurrentUser, selectCurrentUser} from "../users.slice";
import {selectIsDbReady} from "../../database.slice";

export default function UserDetail() {

    const dispatch = useDispatch();
    const isDbReady = useSelector(selectIsDbReady);
    const navigate = useNavigate();
    const user = useSelector(selectCurrentUser);

    const {userId} = useParams();

    // Setup user's details
    useEffect(() => {
        if (isDbReady && !isNaN(Number(userId))) {
            dispatch(fetchUserById(Number(userId)));
        } else if (isDbReady && isNaN(Number(userId))) {
            navigate("/404");
        }

        return () => {
            // Reset current user on component unmount
            dispatch(resetCurrentUser());
        }
    }, [dispatch, isDbReady, userId]);

    useEffect(() => {
        if (isDbReady && user?._id < 0) {
            navigate("/404");
        }
    }, [isDbReady, navigate, user?._id]);

    return (
        <section>
            <div className="container pt-5 user-detail">
                <div className="row justify-content-center">

                    {/*Back button*/}
                    <div className="col-md-1">
                        <button
                            className="btn btn-lg btn-link"
                            onClick={() => navigate(-1)}
                            title="Back"
                        >
                            <i className="bi bi-arrow-left"/>
                        </button>
                    </div>

                    {/*Detail section*/}
                    <div className="col-md-5">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">
                                    <i className="bi bi-person-lines-fill"/>
                                    <span className="ms-1">{user.name} {user.surname}</span>
                                </h4>
                                <p className="card-text"><strong>Age: </strong> {user.age}</p>
                                <p className="card-text"><strong>Gender: </strong>
                                    {user.gender === 'M' ? 'Male' : user.gender === 'F' ? 'Female' : 'Other'}
                                </p>
                                <p className="card-text"><strong>Height: </strong> {user.height} cm</p>
                                <p className="card-text"><strong>User since: </strong>
                                    {
                                        new Date(user.created)
                                            .toLocaleDateString('en-US', {
                                                day: 'numeric',
                                                month: 'long',
                                                year: 'numeric'
                                            })
                                    }
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    );
}
