import React, {useEffect} from "react";
import "./UserForm.css";
import {useForm} from "react-hook-form";
import GenderRadio from "./gender-radio/GenderRadio";
import {capitalize} from "../../../../utils/utils";
import {useDispatch, useSelector} from "react-redux";
import {addUser, resetCurrentUser, selectCurrentUser, updateUser} from "../users.slice";

export default function UserForm() {

    const dispatch = useDispatch();
    const user = useSelector(selectCurrentUser);

    const {formState: {errors, isDirty, isValid}, handleSubmit, register, reset, setValue} = useForm({mode: 'all'});

    // Preload the form with user data if a user is retrieved
    useEffect(() => {
        if (user && user._id) {
            setValue('name', user.name, {
                shouldDirty: true,
                shouldValidate: true
            });
            setValue('surname', user.surname, {
                shouldDirty: true,
                shouldValidate: true
            });
            setValue('age', user.age, {
                shouldDirty: true,
                shouldValidate: true
            });
            setValue('gender', user.gender, {
                shouldDirty: true,
                shouldValidate: true
            });
            setValue('height', user.height, {
                shouldDirty: true,
                shouldValidate: true
            });
        } else {
            reset();
        }
    }, [reset, setValue, user]);

    /**
     * Add user to the list or edit the currently selected one
     * @param data - User's data
     */
    function submit(data: { name: any, surname: any, age: any, gender: any, height: any }) {
        if (user && user._id) {
            dispatch(updateUser({
                ...user,
                name: data.name,
                surname: data.surname,
                age: data.age,
                gender: data.gender,
                height: data.height
            }));
            dispatch(resetCurrentUser());
        } else {
            dispatch(addUser(data));
        }
        reset();
    }

    /**
     * Perform reset operations
     */
    function cancel() {
        reset();
        user && user._id && dispatch(resetCurrentUser());
    }

    return (
        <>
            {/*Header*/}
            <h4 className="mb-4">{user && user._id ? `Editing user #${user._id}` : 'Add a new user'}</h4>

            <form onSubmit={handleSubmit(submit)}>

                {/*Fields*/}
                <div className="mb-3">
                    <input
                        className={`form-control capitalized ${errors.name && 'is-invalid'}`}
                        type="text"
                        id="name"
                        placeholder="Name"
                        {...register("name", {
                            required: "Name is required",
                            setValueAs: name => capitalize(name),
                        })}
                    />
                    {errors.name && <small className="invalid-feedback">{errors.name?.message}</small>}
                </div>
                <div className="mb-3">
                    <input
                        className={`form-control capitalized ${errors.surname && 'is-invalid'}`}
                        type="text"
                        id="surname"
                        placeholder="Surname"
                        {...register("surname", {
                            required: "Surname is required",
                            setValueAs: surname => capitalize(surname),
                        })}
                    />
                    {errors.surname && <small className="invalid-feedback">{errors.surname?.message}</small>}
                </div>
                <div className="mb-3">
                    <input
                        className={`form-control ${errors.age && 'is-invalid'}`}
                        type="text"
                        id="age"
                        placeholder="Age"
                        {...register("age", {
                            min: {
                                message: 'User should be at least 18 years old',
                                value: 18
                            },
                            required: "Age is required",
                            validate: {
                                isNaN: value => isNaN(value) ? 'Please insert a valid age' : true,
                            },
                            valueAsNumber: true
                        })}
                    />
                    {errors.age && <small className="invalid-feedback">{errors.age?.message}</small>}
                </div>
                <div className="mb-3">
                    <GenderRadio register={register} errors={errors}/>
                </div>
                <div className="input-group mb-3">
                    <input
                        className={`form-control ${errors.height && 'is-invalid'}`}
                        type="text"
                        id="height"
                        placeholder="Height (in centimeters)"
                        {...register("height", {
                            min: {
                                message: "User should be at least 50cm tall",
                                value: 50
                            },
                            required: "Height is required",
                            validate: {
                                isNaN: value => isNaN(value) ? 'Height should be a number' : true
                            },
                            valueAsNumber: true
                        })}
                    />
                    <span className="input-group-text">cm</span>
                    {errors.height && <small className="invalid-feedback">{errors.height?.message}</small>}
                </div>

                {/*Buttons*/}
                <div className="d-grid gap-2 d-md-block">
                    <button type="submit" disabled={!isDirty || (isDirty && !isValid)}
                            className="btn btn-success me-md-2">Save
                    </button>
                    <button type="reset" className="btn btn-danger" onClick={cancel}>Cancel</button>
                </div>
            </form>
        </>
    );
}
