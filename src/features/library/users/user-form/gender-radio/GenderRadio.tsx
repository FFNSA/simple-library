import React from "react";
import {UseFormRegister} from "react-hook-form/dist/types/form";
import {FieldErrors, FieldValues} from "react-hook-form";
import "./GenderRadio.css";

interface GenderRadioProps {
    errors: FieldErrors;
    register: UseFormRegister<FieldValues>;
}

export default function GenderRadio({errors, register}: GenderRadioProps) {
    return (
        <>
            <span style={{color: errors.gender ? '#dc3545' : 'inherit'}}>Gender: </span>
            <div className="form-check form-check-inline">
                <input className={`form-check-input ${errors.gender && 'is-invalid'}`}
                       id="gender-m"
                       {...register("gender", {
                           required: "Please select a gender",
                       })}
                       type="radio"
                       value="M"/>
                <label className="form-check-label" htmlFor="gender-m">
                    M
                </label>
            </div>
            <div className="form-check form-check-inline">
                <input className={`form-check-input ${errors.gender && 'is-invalid'}`}
                       id="gender-f"
                       {...register("gender", {
                           required: "Please select a gender"
                       })}
                       type="radio"
                       value="F"/>
                <label className="form-check-label" htmlFor="gender-f">
                    F
                </label>
            </div>
            <div className="form-check form-check-inline">
                <input className={`form-check-input ${errors.gender && 'is-invalid'}`}
                       id="gender-x"
                       {...register("gender", {
                           required: "Please select a gender"
                       })}
                       type="radio"
                       value="X"/>
                <label className="form-check-label" htmlFor="gender-x">
                    Other
                </label>
            </div>
            {
                errors.gender &&
                <small style={{display: errors.gender ? 'block' : 'none'}}
                       className="invalid-feedback">{errors.gender?.message}</small>
            }
        </>
    );
}
