import React from "react";
import "./BooksSearch.css";
import {capitalize} from "../../../../utils/utils";
import {useDispatch} from "react-redux";
import {FormProvider, useForm} from "react-hook-form";
import {BooksActions} from "../store/books.actions";
import UsersSelect from "../book-form/users-select/UsersSelect";

export default function BooksSearch() {

    const dispatch = useDispatch();
    const methods = useForm();
    const {formState: {errors}, handleSubmit, register, reset} = methods;

    /**
     * Perform users search by name and surname (both mandatory)
     * @param data - User's data to query (name and surname)
     */
    function submit(data: { userId: number, name: string, author: string }) {
        dispatch(BooksActions.fetchByNameAuthor(data));
    }

    /**
     * Reset form and restore the original users list
     */
    function cancel() {
        reset();
        dispatch(BooksActions.fetchAll());
    }

    return (
        <div className="row">
            <div className="col-md-12">
                <FormProvider {...methods}>
                    <form onSubmit={handleSubmit(submit)}>
                        <div className="input-group mb-3">
                            <span className="input-group-text">Search</span>
                            <UsersSelect showErrors={false}/>
                            <input aria-label="Title"
                                   className={`form-control ${errors.name && 'is-invalid'}`}
                                   id="name"
                                   placeholder="Title"
                                   type="text"
                                   {...register('name', {
                                       required: true
                                   })}
                            />
                            <input aria-label="Author"
                                   className={`form-control capitalized ${errors.author && 'is-invalid'}`}
                                   id="author"
                                   placeholder="Author"
                                   type="text"
                                   {...register('author', {
                                       required: true,
                                       setValueAs: value => capitalize(value)
                                   })}
                            />
                            <button className="btn btn-outline-secondary" type="submit">
                                <i className="bi bi-search"/>
                            </button>
                            <button className="btn btn-outline-danger" onClick={cancel} type="reset">
                                <i className="bi bi-x-lg"/>
                            </button>
                        </div>
                    </form>
                </FormProvider>
            </div>
        </div>
    );
}
