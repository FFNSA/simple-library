import React from "react";
import "./BooksList.css";
import {Book} from "../../../../models/Book";
import BooksListActions from "./books-list-actions/BooksListActions";
import UsersSelect from "../book-form/users-select/UsersSelect";

interface BooksListProps {
    books: Book[];
}

export default function BooksList({books}: BooksListProps) {
    return (
        <table className="table table-striped table-hover table-responsive books-list">
            <thead className="table-dark">
            <tr className="align-middle">
                <th scope="col">
                    #
                </th>
                <th scope="col">
                    Title
                </th>
                <th scope="col">
                    Author
                </th>
                <th scope="col">
                    <div className="row justify-content-end">
                        <div className="col-8">
                            <UsersSelect placeholder={'Filter by owner'}/>
                        </div>
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            {
                books.map((v, i) =>
                    <tr key={Date.now() + i}>
                        <td>
                            {v._id}
                        </td>
                        <td>
                            {v.name}
                        </td>
                        <td>
                            {v.author}
                        </td>
                        <td>
                            <BooksListActions id={v._id}/>
                        </td>
                    </tr>
                )
            }
            </tbody>
        </table>
    );
}
