import React, {useState} from "react";
import {useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";
import {BooksActions} from "../../store/books.actions";

interface BooksListActionsProps {
    id: number
}

export default function BooksListActions({id}: BooksListActionsProps) {

    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [deletionInProgress, setDeletionInProgress] = useState<boolean>();

    function deleteBook(_id: number) {
        dispatch(BooksActions.deleteBook(_id));
    }

    return (
        <div className="row justify-content-end">
            <div className="col-md-auto">
                {
                    !deletionInProgress
                        // Actions buttons
                        ? <>
                            <button className="btn btn-outline-danger btn-sm me-md-2"
                                    onClick={() => setDeletionInProgress(true)} title="Delete">
                                <i className="bi bi-trash"/>
                            </button>
                            <button className="btn btn-outline-secondary btn-sm"
                                    onClick={() => navigate(`/books/${id}`)}
                                    title="Detail">
                                <i className="bi bi-file-text"/>
                            </button>
                        </>

                        // Deletion confirm buttons
                        : <div className="btn-group" role="group" aria-label="Deletion confirmation">
                            <button className="btn btn-sm btn-outline-success" onClick={() => deleteBook(id)} type="button">
                                <i className="bi bi-check2"/>
                            </button>
                            <button className="btn btn-sm btn-outline-danger" onClick={() => setDeletionInProgress(false)}
                                    type="button">
                                <i className="bi bi-x-lg"/>
                            </button>
                        </div>
                }
            </div>
        </div>
    );
}
