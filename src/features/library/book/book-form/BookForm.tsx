import React from "react";
import "./BookForm.css";
import {capitalize} from "../../../../utils/utils";
import {useDispatch, useSelector} from "react-redux";
import {FormProvider, useForm} from "react-hook-form";
import UsersSelect from "./users-select/UsersSelect";
import {selectUsers} from "../../users/users.slice";
import {BooksActions} from "../store/books.actions";

export default function BookForm() {

    const dispatch = useDispatch();
    const methods = useForm({mode: 'all'});
    const users = useSelector(selectUsers);

    const {formState: {errors, isDirty, isValid}, handleSubmit, register, reset} = methods;

    /**
     * Add book to the list or edit the currently selected one
     * @param data - Book's data
     */
    function submit(data: any) {
        dispatch(BooksActions.addBook(data));
        reset();
    }

    return (
        <>
            {/*Header*/}
            <h4 className="mb-4">Add a new book</h4>

            {
                !users.length
                    ?
                    // Alert preventing the creation of a book without an owner
                    <div className="alert alert-info" role="alert">Please create at least one user before adding a book
                        to the database
                    </div>
                    // Book creation form
                    : <FormProvider {...methods}>
                        <form onSubmit={handleSubmit(submit)}>

                            {/*Fields*/}
                            <div className="mb-3">
                                <input
                                    className={`form-control ${errors.name && 'is-invalid'}`}
                                    type="text"
                                    id="title"
                                    placeholder="Title"
                                    {...register("name", {
                                        required: "Title is required"
                                    })}
                                />
                                {errors.name && <small className="invalid-feedback">{errors.name?.message}</small>}
                            </div>
                            <div className="input-group mb-3">
                                <span className="input-group-text">$</span>
                                <input
                                    className={`form-control ${errors.price && 'is-invalid'}`}
                                    type="text"
                                    id="price"
                                    placeholder="Price"
                                    {...register("price", {
                                        required: "Price is required",
                                        validate: {
                                            isNaN: value => isNaN(value) ? 'Please insert a valid price' : true,
                                        },
                                        valueAsNumber: true
                                    })}
                                />
                                {errors.price && <small className="invalid-feedback">{errors.price?.message}</small>}
                            </div>
                            <div className="mb-3">
                                <input
                                    className={`form-control capitalized ${errors.author && 'is-invalid'}`}
                                    type="text"
                                    id="author"
                                    placeholder="Author"
                                    {...register("author", {
                                        required: "Author is required",
                                        setValueAs: author => capitalize(author)
                                    })}
                                />
                                {errors.author && <small className="invalid-feedback">{errors.author?.message}</small>}
                            </div>
                            <div className="mb-3">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    id="is-ebook"
                                    {...register("isEbook")}
                                />
                                <label className="form-check-label" htmlFor="is-ebook">
                                    Is ebook?
                                </label>
                            </div>
                            <div className="mb-3">
                                <UsersSelect/>
                            </div>

                            {/*Buttons*/}
                            <div className="d-grid gap-2 d-md-block">
                                <button type="submit" disabled={!isDirty || (isDirty && !isValid)}
                                        className="btn btn-success me-md-2">Save
                                </button>
                                <button type="reset" className="btn btn-danger" onClick={() => reset()}>Cancel</button>
                            </div>
                        </form>
                    </FormProvider>
            }
        </>
    );
}
