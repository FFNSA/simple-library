import React, {ChangeEvent} from "react";
import "./UserSelect.css";
import {useFormContext} from "react-hook-form";
import {useDispatch, useSelector} from "react-redux";
import {BooksActions} from "../../store/books.actions";
import {selectUsers} from "../../../users/users.slice";

interface UsersSelectProps {
    placeholder?: string
    showErrors?: boolean;
}

export default function UsersSelect({placeholder, showErrors = true}: UsersSelectProps) {

    const dispatch = useDispatch();
    const users = useSelector(selectUsers);

    function updateBooks(e: ChangeEvent<HTMLSelectElement>) {
        const id = Number(e.currentTarget.value);

        if (!isNaN(id) && id > 0) {
            dispatch(BooksActions.fetchByUserId(id));
        } else {
            dispatch(BooksActions.fetchAll());
        }
    }

    try {
        // Case A - component included in a form context

        // eslint-disable-next-line react-hooks/rules-of-hooks
        const {formState: {errors, isDirty}, register} = useFormContext();

        return (
            <>
                <select
                    aria-label="Users select"
                    className={`form-select user-select ${errors?.userId && 'is-invalid'}`}
                    {...register('userId', {
                        required: 'Please select the owner of the book',
                        min: {
                            message: 'Please select an owner for the book',
                            value: 1
                        },
                        validate: {
                            isNaN: value => isNaN(value) ? 'Please select an owner for the book' : true,
                        },
                        valueAsNumber: true,
                        value: '0'
                    })}
                >
                    <option disabled={isDirty} value="0">{placeholder || 'Select the owner of the book'}</option>
                    {
                        users.map(u => {
                            // TODO: possible bug(?) - if Date.now() is used as part of the key, the UI "lags" and the wrong value is selected
                            return <option key={u._id} value={u._id}>{u.name} {u.surname}</option>
                        })
                    }
                </select>
                {
                    errors.userId && showErrors &&
                    <small style={{display: errors.userId ? 'block' : 'none'}}
                           className="invalid-feedback">{errors.userId?.message}</small>
                }
            </>
        );
    } catch (e) {
        // Case B - component not included in a form context
        return (
            <select aria-label="Users select"
                    className="form-select form-select-sm user-select"
                    onChange={updateBooks}
                    defaultValue="0"
            >
                <option value="0">{placeholder || 'Select the owner of the book'}</option>
                {
                    users.map(u => {
                        return <option key={u._id} value={u._id}>{u.name} {u.surname}</option>
                    })
                }
            </select>
        );
    }
}
