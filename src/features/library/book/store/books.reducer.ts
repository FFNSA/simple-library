import {createReducer} from "@reduxjs/toolkit";
import {Book} from "../../../../models/Book";
import {BooksActions} from "./books.actions";

const booksReducer = createReducer({
    current: {} as Book,
    list: [] as Book[]
}, builder => {
    builder
        .addCase(BooksActions.addBook.fulfilled, (state, {payload}) => {
            state.list.push(payload);
        })
        .addCase(BooksActions.deleteBook.fulfilled, (state, {payload}) => {
            state.list = state.list.filter(b => b._id !== payload);
        })
        .addCase(BooksActions.fetchAll.fulfilled, (state, {payload}) => {
            state.list = payload;
        })
        .addCase(BooksActions.fetchById.fulfilled, (state, {payload}) => {
            state.current = payload;
        })
        .addCase(BooksActions.fetchByNameAuthor.fulfilled, (state, {payload}) => {
            state.list = payload;
        })
        .addCase(BooksActions.fetchByUserId.fulfilled, (state, {payload}) => {
            state.list = payload;
        })
        .addCase(BooksActions.resetCurrentBook, (state) => {
            state.current = {} as Book;
        })
});

export default booksReducer;
