import {Book} from "../../../../models/Book";

export const selectCurrentBook = (state: { books: { current: Book; } }) => state.books.current;
export const selectBooks = (state: { books: { list: Book[]; } }) => state.books.list;
