import {createAction, createAsyncThunk} from "@reduxjs/toolkit";
import {Book} from "../../../../models/Book";
import {api} from "../../../../assets/js/mockend";

enum actions {
    addBook = 'books/add',
    deleteBook = 'books/delete',
    fetchAll = 'books/fetchAll',
    fetchById = 'books/fetchById',
    fetchByNameAuthor = 'books/fetchByAuthor',
    fetchByUserId = 'books/fetchByUserId',
    resetCurrentBook = 'books/resetCurrentBook'
}

/**
 * Thunk for adding a book to the database
 */
const addBook = createAsyncThunk(actions.addBook, async (book: Book) => {
    try {
        const result = await api.books.addBook({...book});
        return {...book, _id: result._id};
    } catch (e) {
        throw e;
    }
});

/**
 * Thunk for deleting a book from database
 */
const deleteBook = createAsyncThunk(actions.deleteBook, async (_id: number) => {
    try {
        const result = await api.books.deleteBookById({_id});
        if (result) {
            return _id;
        }
    } catch (e) {
        throw e;
    }
});

/**
 * Thunk for fetching all books from database
 */
const fetchAll = createAsyncThunk(actions.fetchAll, async () => {
    try {
        const books = await api.books.getAllBooks();
        return books.map((book: Book) => ({...book, created: book.created.getTime()}));
    } catch (e) {
        throw e;
    }
});

/**
 * Thunk for fetching all books from a specified author
 */
const fetchByNameAuthor = createAsyncThunk(actions.fetchByNameAuthor,
    async (params: { userId: number, name: string, author: string }) => {
        try {
            const books = await api.books.getBookByNameAuthor({...params});
            return books.map((book: Book) => ({...book, created: book.created.getTime()}));
        } catch (e) {
            throw e;
        }
    }
);

/**
 * Thunk for fetching a book from its id
 */
const fetchById = createAsyncThunk(actions.fetchById, async (_id: number) => {
    try {
        const book = await api.books.getBookById({_id});
        book.created = book.created.getTime();
        return book;
    } catch (e) {
        return {_id: -1};
    }
});

/**
 * Thunk from fetching all the books of a specified user
 */
const fetchByUserId = createAsyncThunk(actions.fetchByUserId, async (userId: number) => {
    try {
        const books = await api.books.getBooksByUserId({userId});
        return books.map((book: Book) => ({...book, created: book.created.getTime()}))
    } catch (e) {
        throw e;
    }
});

/**
 * Action for resetting the current selected book
 */
const resetCurrentBook = createAction(actions.resetCurrentBook, () => {
    return {
        payload: {}
    }
})

export const BooksActions = {
    addBook,
    deleteBook,
    fetchAll,
    fetchByNameAuthor,
    fetchById,
    fetchByUserId,
    resetCurrentBook
};
