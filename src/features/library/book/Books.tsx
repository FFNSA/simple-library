import React, {useEffect} from "react";
import "./Books.css";
import {useDispatch, useSelector} from "react-redux";
import {selectBooks} from "./store/book.selectors";
import {selectIsDbReady} from "../database.slice";
import {BooksActions} from "./store/books.actions";
import BookForm from "./book-form/BookForm";
import {fetchUsers} from "../users/users.slice";
import BooksSearch from "./books-search/BooksSearch";
import BooksList from "./books-list/BooksList";

export default function Books() {

    const books = useSelector(selectBooks);
    const dispatch = useDispatch();
    const isDbReady = useSelector(selectIsDbReady);

    useEffect(() => {
        if (isDbReady) {
            dispatch(fetchUsers());
            dispatch(BooksActions.fetchAll());
        }
    }, [dispatch, isDbReady]);


    return (
        <section>
            <div className="container pt-5">
                <div className="row justify-content-center">
                    <div className="col">
                        <div className="row">
                            <div className="col-4">
                                <BookForm/>
                            </div>
                            <div className="col-8">
                                <BooksSearch/>
                                {
                                    !books.length
                                        ?
                                        <div className="alert alert-info" role="alert">No books currently in
                                            database
                                        </div>
                                        : <BooksList books={books}/>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
