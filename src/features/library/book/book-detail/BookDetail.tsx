import React, {useEffect} from "react";
import "./BookDetail.css";
import {useNavigate, useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {selectCurrentBook} from "../store/book.selectors";
import {selectIsDbReady} from "../../database.slice";
import {BooksActions} from "../store/books.actions";
import {fetchUserById, resetCurrentUser, selectCurrentUser} from "../../users/users.slice";

export default function BookDetail() {

    const book = useSelector(selectCurrentBook);
    const dispatch = useDispatch();
    const isDbReady = useSelector(selectIsDbReady);
    const navigate = useNavigate();
    const owner = useSelector(selectCurrentUser);

    const {bookId} = useParams();

    // Setup book's details
    useEffect(() => {
        if (isDbReady && !isNaN(Number(bookId))) {
            dispatch(BooksActions.fetchById(Number(bookId)));
        } else if (isDbReady && isNaN(Number(bookId))) {
            navigate("/404");
        }

        // Reset current book on component unmount
        return () => {
            dispatch(BooksActions.resetCurrentBook());
        }
    }, [bookId, dispatch, isDbReady, navigate]);

    // Setup book owner's details
    useEffect(() => {
        if (isDbReady && !!book.userId) {
            dispatch(fetchUserById(book.userId));
        }

        // Check if the book if valid. If not redirect the user to 404 page
        if (isDbReady && book._id < 0) {
            navigate("/404");
        }

        // Reset current user on component unmount
        return () => {
            dispatch(resetCurrentUser());
        }
    }, [book._id, book.userId, dispatch, isDbReady, navigate]);


    return (
        <section>
            <div className="container pt-5 book-detail">
                <div className="row justify-content-center">

                    {/*Back button*/}
                    <div className="col-md-1">
                        <button
                            className="btn btn-lg btn-link"
                            onClick={() => navigate(-1)}
                            title="Back"
                        >
                            <i className="bi bi-arrow-left"/>
                        </button>
                    </div>

                    {/*Detail section*/}
                    <div className="col-md-5">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">
                                    <i className="bi bi-book"/>
                                    <span className="ms-1">{book.name}</span>
                                </h4>
                                <p className="card-text"><strong>Author: </strong> {book.author}</p>
                                <p className="card-text"><strong>Price: </strong> ${book.price}</p>
                                <p className="card-text"><strong>Is ebook: </strong> {book.isEbook ? 'Yes' : 'No'}</p>
                                <p className="card-text"><strong>Owner: </strong> {owner.name} {owner.surname}</p>
                                <p className="card-text"><strong>Added on: </strong>
                                    {
                                        new Date(book.created)
                                            .toLocaleDateString('en-US', {
                                                day: 'numeric',
                                                month: 'long',
                                                year: 'numeric'
                                            })
                                    }
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
