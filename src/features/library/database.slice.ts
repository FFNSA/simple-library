import {createSlice} from "@reduxjs/toolkit";
import init from "../../assets/js/mockend";

export const databaseSlice = createSlice({
    name: 'db',
    initialState: {
        ready: false
    },
    reducers: {
        setReadyState: (state, {payload}) => {
            state.ready = payload;
        }
    }
});

/**
 * Thunk for database initialization
 */
export const loadDatabase = () => {
    return async (dispatch: (fn: { payload: boolean; type: string; }) => void, getState: () => any) => {
        try {
            const wasReady = getState().ready;
            const setReadyState = databaseSlice.actions.setReadyState;

            // Do not initialize the database more than one
            if (!wasReady) {
                const status = await init();
                dispatch(setReadyState(status));
            }
        } catch (e) {
            console.error('[DB] ', e);
        }
    };
}

// Selector
export const selectIsDbReady = (state: { db: { ready: boolean; }; }) => state.db.ready;

export default databaseSlice.reducer;
