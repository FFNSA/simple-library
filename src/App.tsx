import React, {useEffect} from 'react';
import './App.css';
import Header from "./features/main/header/Header";
import Footer from "./features/main/footer/Footer";
import {Outlet, Route, Routes, useLocation, useNavigate} from "react-router-dom";
import Nav from "./features/main/nav/Nav";
import Login from "./features/login/Login";
import Users from "./features/library/users/Users";
import Books from "./features/library/book/Books";
import {useDispatch, useSelector} from "react-redux";
import {authenticate, selectAuthAttempts, selectAuthState} from "./features/login/auth.slice";
import {loadDatabase} from "./features/library/database.slice";
import UserDetail from "./features/library/users/user-detail/UserDetail";
import BookDetail from "./features/library/book/book-detail/BookDetail";
import NotFound from "./features/misc/404/NotFound";

function App() {

    const dispatch = useDispatch();

    const authAttempts = useSelector(selectAuthAttempts);
    const isAuthenticated = useSelector(selectAuthState);
    // const isDbReady = useSelector(selectIsDbReady);
    const location = useLocation();
    const navigate = useNavigate();

    // Initialize database and restore previous user's session (if present)
    useEffect(() => {
        dispatch(authenticate());
        dispatch(loadDatabase());
    }, [dispatch]);

    // Configure redirects based on user's status and location
    useEffect(() => {
        if (!isAuthenticated && authAttempts) {
            navigate('/login');
        } else {
            if (location.pathname === '/') {
                navigate('/users');
            }
        }
    }, [authAttempts, isAuthenticated, location.pathname, navigate]);

    return (
        <>
            <Header/>
            <Nav/>
            <main>
                <Routes>
                    <Route path="/" element={<Outlet/>}>
                        <Route path="login" element={<Login/>}/>
                        <Route path="users">
                            <Route index element={<Users/>}/>
                            <Route path=":userId" element={<UserDetail/>}/>
                        </Route>
                        <Route path="books">
                            <Route index element={<Books/>}/>
                            <Route path=":bookId" element={<BookDetail/>}/>
                        </Route>
                        <Route path="404" element={<NotFound/>}/>
                        {/*Not using the following code because it makes the UI flicker*/}
                        {/*{*/}
                        {/*    isDbReady && <>*/}
                        {/*        <Route path="users" element={<Users/>}/>*/}
                        {/*        <Route path="books" element={<Books/>}/>*/}
                        {/*    </>*/}
                        {/*}*/}
                    </Route>
                    {/*Fallback Route*/}
                    {/*<Route path="*" element={*/}
                    {/*    <section style={{padding: "1rem"}}>*/}
                    {/*        <h1>404: Page not found!</h1>*/}
                    {/*    </section>*/}
                    {/*}/>*/}
                    <Route path="*" element={<NotFound/>}/>
                </Routes>
            </main>
            <Footer/>
        </>
    );
}

export default App;
