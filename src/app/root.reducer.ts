import {combineReducers} from "@reduxjs/toolkit";
import authReducer from "../features/login/auth.slice";
import dbReducer from "../features/library/database.slice";
import booksReducer from "../features/library/book/store/books.reducer";
import usersReducer from "../features/library/users/users.slice";

const rootReducer = combineReducers({
    auth: authReducer,
    db: dbReducer,
    books: booksReducer,
    users: usersReducer
});

export default rootReducer;
